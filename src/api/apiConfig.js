const apiConfig = {
    baseUrl: 'https://api.themoviedb.org/3/',
    apiKey: '67a020e9e51ac0c4d343109de065a2c5',
    originalImage: (imgPath) => `https://image.tmdb.org/t/p/original/${imgPath}`,
    w500Image: (imgPath) => `https://image.tmdb.org/t/p/w500/${imgPath}`
}

export default apiConfig;
