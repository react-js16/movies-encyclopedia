import React from 'react';

import '../hero-slide/hero-slide.scss';

import Button, {OutlineButton} from '../button/Button';

const HeroText = () => {
    return (
        <>
            <div
                className={`hero-slide__item`}
                style={{backgroundImage: `url()`}}
            ></div>
            <div className="hero-slide__item__content container">
                <div className="hero-slide__item__content__info">
                    <h2 className="title"></h2>
                    <div className="overview"></div>
                    <div className="btns">
                        <Button>
                            Watch now
                        </Button>
                        <OutlineButton>
                            Watch trailer
                        </OutlineButton>
                    </div>
                </div>
                {/* <div className="hero-slide__item__content__poster">
                    <img src={apiConfig.w500Image(item.poster_path)} alt="" />
                </div> */}
            </div>
        </>
    );
};

export default HeroText;