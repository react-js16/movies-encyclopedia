import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import './movie-list.scss';

import { SwiperSlide, Swiper } from 'swiper/react';

import MovieCard from '../movie-card/MovieCard';

import tmdbApi, { category } from '../../api/tmdbApi';

const MovieList = props => {

    const [items, setItems] = useState([]);
    useEffect(() => {
        const getList = async () => {
            let response = null;

            if(props.type !== 'similar') {
                switch(props.category) {
                    case category.movie:
                        response = await tmdbApi.getMoviesList(props.type, {params: {
                            region: 'US',
                            include_adult: false,
                            primary_release_year: '2021',
                        }});
                        break;
                    default:
                        response = await tmdbApi.getTvList(props.type, {params: {
                            region: 'US',
                            include_adult: false,
                            primary_release_year: '2022',
                            without_genres: ['10749', '18', '10764'],
                        }});
                }
            } else {
                response = await tmdbApi.similar(props.category, props.id);
            }
            setItems(response.results)
            console.log(response)
        }
        getList();
    }, [props.category, props.type, props.id])

    return (
        <div className='movie-list'>
            <Swiper
                grabCursor={true}
                slidesPerView={'auto'}
                spaceBetween={20}
            >
                {
                    items.map((item, i) => (
                        <SwiperSlide key={i}>
                            <MovieCard item={item} category={props.category}/>
                        </SwiperSlide>
                    ))
                }
            </Swiper>
        </div>
    );
};

MovieList.propTypes = {
    category: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
};

export default MovieList;