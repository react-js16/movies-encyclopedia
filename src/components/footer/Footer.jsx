import React from 'react';

import { Link } from 'react-router-dom';

import './footer.scss';

import bg from '../../assets/footer-bg.jpg';
import logo from '../../assets/ME.png';

const Footer = () => {
    return (
        <div className='footer' style={{backgroundImage: `url(${bg})`}}>
            <div className="footer_content container">
                <div className="footer__content__logo">
                    <div className="logo">
                        <img src={logo} alt="" />
                        <Link to="/">Movies Encyclopedia</Link>
                    </div>
                </div>
                <div className="footer__content__menus">
                    <div className="footer__content__menu">
                        <Link to='/'>Home</Link>
                        <Link to='/'>Contact Us</Link>
                        <Link to='/'>Term Of Services</Link>
                        <Link to='/'>About Us</Link>
                    </div>

                    <div className="footer__content__menu">
                        <Link to='/'>Live</Link>
                        <Link to='/'>FAQ Us</Link>
                        <Link to='/'>Premium</Link>
                        <Link to='/'>Privacy Policy</Link>
                    </div>

                    <div className="footer__content__menu">
                        <Link to='/'>You Must Watch</Link>
                        <Link to='/'>Recent Relese</Link>
                        <Link to='/'>Top IMDB</Link>
                    </div>
                </div>
                <div className="footer__content__author">
                    Author: Salman Al-Majali
                </div>
            </div>
        </div>
    );
};

export default Footer;